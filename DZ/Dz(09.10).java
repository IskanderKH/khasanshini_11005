// В отсортированном массиве найти элемент с помощью бинарного поиска
import java.util.Arrays;

public class BS {
    public static void main(String[] args) {
        int[] arr = new int[] {1, 5, 14, 16, 25, 34, 58, 109, 421, 457, 809};
        System.out.println(Arrays.toString(arr));
        System.out.println(binarySearch(arr, 34));
    }

    public static int binarySearch(int[] array, int key ) {
        int start = 0;
        int end = array.length -1;

        while (start <= end) {
            int op = start + (end - start) / 2;
            if (key < array[op]) {
                end = op - 1;
            } 
            else if (key > array[op]) {
                start = op + 1;
            } 
            else {
                return op;
            }
        }
        return -1;
    }
}