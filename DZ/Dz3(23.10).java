// Рекурсивно вычислить число Фибоначчи с номером N
import java.util.Scanner;

public class recurs {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(rec(n));

    }

    public static int rec(int n) {
        if (n == 0) {
            return 0;
        } else
            if (n==1) {
            	return 1;
        } else {
                return  rec(n - 1) + rec(n - 2);
            }

    }
}
