// Рекурсивно посчитать произведение чисел от 1 до N.


import java.util.Scanner;

public class Recursion {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(n + "!= " + recursion(n));
    }

    public static int recursion(int n) {

        if (n == 1 || n == 0) {
            return 1;
        }
        return recursion(n-1) * n;
    }
}