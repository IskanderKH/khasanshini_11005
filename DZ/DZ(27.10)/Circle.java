public class Circle extends Shape {
    private double r;

    public Circle(double r){
        this.r = r;
    }

    public void getArea(){
        area = r * r * 3.14;
    }

}