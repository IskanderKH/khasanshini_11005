import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(0, 0);
        Triangle triangle = new Triangle(0, 0);
        Circle circle = new Circle(0);
        System.out.println("--------------------------------------------------");
        System.out.println("Сколько площадей хотите найти?\n" + "Введите число");

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        Shape[] figures = new Shape[n];

        if (n < 1){
            System.out.println("ERROR\n" + "Число должно быть больше нуля");
            return;
        }
        System.out.println("--------------------------------------------------");
        System.out.println("Вы можете найти площадь следующей фигуры:\n"
                + "1.Прямоугольник\n" + "2.Треугольник\n" + "3.Круг");


        for (int i=0; i<n; i++){
            System.out.println("Введите номер фигуры, площадь которой хотите найти");
            String x = sc.next();
            System.out.println("--------------------------------------------------");
            do{
                if(x.equals("1")){
                    System.out.println("Введите длину и ширину прямоугольника");
                    figures[i] = new Rectangle(sc.nextDouble(), sc.nextDouble());
                    break;
                }
                else if(x.equals("2")){
                    System.out.println("Введите основание и высоту треугольника");

                    figures[i] = new Triangle(sc.nextDouble(), sc.nextDouble());
                    break;
                }
                else if(x.equals("3")){
                    System.out.println("Введите радиус круга");
                    figures[i] = new Circle(sc.nextDouble());
                    break;
                }
                else
                    System.out.println("Можно ввести только указынные цифры");
                x = sc.next();
            }while(true);
            figures[i].getArea();
        }

        System.out.println( "--------------------------------------------------\n" +
                            "Площадь фигуры:");
        for (int i = 0; i < n; i++) {
            if(figures[i].getClass().equals(rectangle.getClass()))
                System.out.println("Прямоугольник: " + figures[i].area);
            else if(figures[i].getClass().equals(circle.getClass()))
                System.out.println("Круг: " + figures[i].area);
            else
                System.out.println("Треугольник: " + figures[i].area);
        }

    }
}
