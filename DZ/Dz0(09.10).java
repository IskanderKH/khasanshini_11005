// Реализовать быструю сортировку (quicksort)

import java.util.Arrays;

public class QS {

    public static void quickSort(int[] array, int start, int end) {
        if (array.length == 0)
            return;

        if (start >= end)
            return;
        int i = start;
        int j = end;

        int op = start + (end - start) / 2;

        while (i <= j) {
            while (array[i] < array[op]) {
                i++;
            }
            while (array[j] > array[op]) {
                j--;
            }

            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }

        if (start < j)
            quickSort(array, start, j);

        if (end > i)
            quickSort(array, i, end);
    }
    public static void main(String[] args) {
        int[] arr = { 4, 594, 76, 12, 1, 5, 709, 5431, 89, 4 };
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
        int start = 0;
        int end = arr.length - 1;

        quickSort(arr, start, end);
        System.out.println("-----------------");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        };
    }
}