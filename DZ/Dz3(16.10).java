// Написать метод, который находит максимальное значение массива.
import java.util.Arrays;

public class Test3 {

    public static void main(String[] args) {
            int[] arr = new int[]{15,56,0,14,36,23,109,2,80,176};
            for(int i = 0; i < arr.length; i++) {
                System.out.println(arr[i]);
            }
            maxV(arr);
        }


        public static void maxV(int[] array ) {
            int maxVo = array[0];

            for (int i = 1; i < array.length; i++) {
                if (array[i] > maxVo) {
                    maxVo = array[i];
                }
            }
            System.out.println("max = " + maxVo);
            
        }
}