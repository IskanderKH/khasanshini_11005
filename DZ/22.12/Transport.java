import java.util.*;

public class Transport {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Route route = new Route();
        Generator generator = new Generator();
        System.out.println("Введите количество автобусов:");
        int autobusAmount = in.nextInt();
        route.setAr((ArrayList<Object>) generator.generateList("Autobus", autobusAmount));
        System.out.println("Введите количество троллейбусов:");
        int trolleyAmount = in.nextInt();
        route.addAr(route.getAr(), (ArrayList<Object>) generator.generateList("Trolleybus", trolleyAmount));
        System.out.println("Введите количество трамваев:");
        int tramwayAmount = in.nextInt();
        route.addAr(route.getAr(), (ArrayList<Object>) generator.generateList("Tramway", tramwayAmount));

        HashMap<String, List<Integer>> map = new HashMap<>();
        ArrayList<Integer> ar = new ArrayList<>();


        for (int i = 0; i < route.getAr().size(); i++) {
            var key = route.getAr().get(i).getClass().getSimpleName(); // Тип
            if (key.equals("Autobus")) {
                Autobus autobus = (Autobus) route.getAr().get(i);
                ar.add(autobus.getInterval());
                List<Integer> list;
                try {
                    list = map.get(key);
                    list.add(ar.get(i));
                    map.put(key, list);
                } catch (NullPointerException e) {
                    list = new ArrayList<>();
                    list.add(ar.get(i));
                    map.put(key, list);
                }
            }
            if (key.equals("Trolleybus")) {
                Trolleybus trolleybus = (Trolleybus) route.getAr().get(i);
                ar.add(trolleybus.getInterval());
                List<Integer> list;
                try {
                    list = map.get(key);
                    list.add(ar.get(i));
                    map.put(key, list);
                } catch (NullPointerException e) {
                    list = new ArrayList<>();
                    list.add(ar.get(i));
                    map.put(key, list);
                }
            }
            if (key.equals("Tramway")) {
                Tramway tramway = (Tramway) route.getAr().get(i);
                ar.add(tramway.getInterval());
                List<Integer> list;
                try {
                    list = map.get(key);
                    list.add(ar.get(i));
                    map.put(key, list); // Добавляем в HashMap наш транспорт и его интервалы
                } catch (NullPointerException e) {
                    list = new ArrayList<>();
                    list.add(ar.get(i));
                    map.put(key, list);
                }
                map.put(key, list);
            }
        }
        System.out.println("Введите транспорт, который сломается или '0' для вывода всех транспортов и интервалов");
        String s = in.next();
        if (s.charAt(0) == '0') {
            for (String key: map.keySet()) {
                System.out.println("Транспорт: " + key);
                System.out.println("Интервал: " + map.get(key));
            }
            System.exit(0);
        }
        if (map.containsKey(s)) {
            System.out.println("Введите номер транспорта от 1 до " + map.get(s).size());
            int x = in.nextInt();
            if (x <= map.get(s).size() && x >= 1) {
                List<Integer> list;
                int length = map.get(s).size();
                String q = map.get(s).toString();

                int a = route.modaValue(map.get(s));
                map.get(s).remove(x - 1);
                list = map.get(s);
                map.put(s, list);
                System.out.println("Интервалы данного транспорта были: ");
                System.out.println(q);
                System.out.println("Среднее значение:");
                System.out.println(a);
                System.out.println("Для сохранности времени они стали:");
                list.clear();
                for (int i = 0; i < length; i++) {
                    list.add(a);
                }
                map.put(s, list);
                System.out.println(map.get(s));
            } else {
                System.out.println("Вы ввели неправильный диапазон!");
            }
        }
        for (String key: map.keySet()) {
            System.out.println("Транспорт: " + key);
            System.out.println("Интервал: " + map.get(key));
        }
    }
}