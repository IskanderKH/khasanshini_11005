import java.util.ArrayList;
import java.util.List;

public class Route {
    private ArrayList<Object> ar = new ArrayList<>();

    public ArrayList<Object> getAr() {
        return ar;
    }

    public void setAr(ArrayList<Object> ar) {
        this.ar = ar;
    }

    public void addAr(ArrayList<Object> ar, ArrayList<Object> ar2) {
        ar.addAll(ar2);
        this.ar = ar;
    }

    public int modaValue(List<Integer> values) {
        int moda = 0;
        for (Integer value : values) {
            moda += value;
        }
        moda = moda / values.size();
        return moda;
    }

}

