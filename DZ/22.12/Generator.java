import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Generator {
    private Object type;
    private int amount;

    public List<Object> generateList(String type, int amount) { //  Генерируем List объектов
        List<Object> list = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            Random random = new Random();
            if (type.equals("Autobus")) {
                Autobus autobus = new Autobus(random.nextInt(20) + 1);
                list.add(autobus);
            }
            if (type.equals("Trolleybus")) {
                Trolleybus trolleybus = new Trolleybus(random.nextInt(20) + 1);
                list.add(trolleybus);
            }
            if (type.equals("Tramway")) {
                Tramway tramway = new Tramway(random.nextInt(20) + 1);
                list.add(tramway);
            }
        }
        return list;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }
}
