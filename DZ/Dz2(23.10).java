// Рекурсивно вывести целые числа от A до B (A < B)

import java.util.Scanner;

public class Rec {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        System.out.println(recurs( a,b));
    }

    public static String recurs(int a, int b) {

        if ( a == b) {
            return Integer.toString(a);
        }
        return a + " " + recurs(a+1,b) ;
    }
}