// Вычислить определитель матрицы (2х2, 3х3).

public class sss {
    public static void main(String[] args) {
        int[][] matrixA;
        matrixA = new int[2][2];

        matrixA[0][0] = 1;
        matrixA[0][1] = -2;
        matrixA[1][0] = 4;
        matrixA[1][1] = 1;

        System.out.println("Matrix A:");
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                System.out.print(matrixA[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        System.out.print( "det A=" + ((matrixA[0][0] * matrixA[1][1]) - (matrixA[0][1] * matrixA[1][0])) );
        }
    }



public class hhh {
    public static void main(String[] args) {
        int[][] matrixB;
        matrixB = new int[3][3];

        matrixB[0][0] = 1;
        matrixB[0][1] = -2;
        matrixB[0][2] = 5;
        matrixB[1][0] = 4;
        matrixB[1][1] = 1;
        matrixB[1][2] = 7;
        matrixB[2][0] = 3;
        matrixB[2][1] = 8;
        matrixB[2][2] = 9;

        System.out.println("Matrix B:");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(matrixB[i][j] + "\t");
            }
            System.out.println();
        }
        int x1 = ((matrixB[0][0]*matrixB[1][1]*matrixB[2][2])+(matrixB[0][1]*matrixB[2][0]*matrixB[1][2])+
                (matrixB[1][0]*matrixB[2][1]*matrixB[0][2]));
        int x2 = ((matrixB[2][0]*matrixB[1][1]*matrixB[0][2])+(matrixB[1][0]*matrixB[0][1]*matrixB[2][2])+
                (matrixB[0][0]*matrixB[1][2]*matrixB[2][1]));
        int x3 = (x1 - x2);
        System.out.println();
        System.out.print( "det B=" + x3 );
    }
}