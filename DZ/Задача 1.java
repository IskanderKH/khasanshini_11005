// Реализовать сложение матриц одинакового размера (не фиксированного).
    public class Sum {

        public static void main(String [] args) {
            int n = 2;
            int [][] a = new int [n][n];
            int [][] b = new int [n][n];
            int [][] c = new int [n][n];

            System.out.println(" matrix a:");
            for(int i = 0; i < a.length; i++) {
                for(int j = 0; j < a[i].length; j++) {
                    a[i][j] = (int) (Math.random() * 100);
                    System.out.print(" " + a[i][j]);
                }
                System.out.println();
            }
            System.out.println("\n matrix b:");
            for(int i = 0; i < b.length; i++) {
                for(int j = 0; j < b[i].length; j++) {
                    b[i][j] = (int) (Math.random() * 100);
                    System.out.print(" " + b[i][j]);
                }
                System.out.println();
            }
            System.out.println("\n sum: ");
            for(int i = 0; i < c.length; i++) {
                for(int j = 0; j < c[i].length; j++) {
                    c[i][j] = a[i][j] + b[i][j];
                    System.out.print(" " + c[i][j]);
                }
                System.out.println();
            }
        }
    }

