// Реализовать умножение матриц (алгоритм не должен зависеть от размера матрицы).
import java.util.Scanner;

public class Dz1 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Horizontal and Vertical");
        int n=sc.nextInt();
        int m=sc.nextInt();
        int[][] a = new int[n][m];
        int[][] b = new int[m][n];
        int[][] c = new int[n][n];
        System.out.println();
        System.out.println("Matrix a");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j]=sc.nextInt();
            }
        }
        System.out.println();
        System.out.println(" Matrix b");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                b[i][j]=sc.nextInt();
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < m; k++) {
                    c[i][j]+=a[i][k]*b[k][j];
                }
            }
        }
        System.out.println();
        System.out.println("Result:");
        for (int i = 0; i < n; i++) {
            System.out.println();
            for (int j = 0; j < n; j++) {
                System.out.print(c[i][j]+" ");
            }
        }
    }
}
